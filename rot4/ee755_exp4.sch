<Qucs Schematic 0.0.19>
<Properties>
  <View=226,117,1584,829,0.839055,0,0>
  <Grid=10,10,1>
  <DataSet=ee755_exp4.dat>
  <DataDisplay=ee755_exp4.dpl>
  <OpenDisplay=1>
  <Script=ee755_exp4.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 1200 260 0 64 0 0 "lin" 1 "245 MHz" 1 "735 MHz" 1 "19" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <SUBST Subst1 1 1220 520 -30 24 0 0 "4.2" 1 "1.6 mm" 1 "17 um" 1 "2e-2" 1 "0.022e-6" 1 "0.15e-6" 1>
  <MLIN MS9 1 880 550 15 -26 0 1 "Subst1" 1 "1.7 mm" 1 "15 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS6 1 750 650 -26 15 0 0 "Subst1" 1 "1.7 mm" 1 "73 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCORN MS5 1 880 650 15 -26 1 0 "Subst1" 1 "1.7 mm" 1>
  <MLIN MS8 1 880 340 15 -26 0 1 "Subst1" 1 "1.7 mm" 1 "15 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCORN MS3 1 880 230 15 -7 0 0 "Subst1" 1 "1.7 mm" 1>
  <MLIN MS1 1 750 230 -26 15 0 0 "Subst1" 1 "1.7 mm" 1 "73 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MTEE MS2 1 880 460 -96 -45 0 1 "Subst1" 1 "1.7 mm" 1 "1.7 mm" 1 "3.2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0 "showNumbers" 0>
  <GND * 1 1090 560 0 0 0 0>
  <Pac P1 1 1090 490 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <MLIN MS10 1 1020 460 -26 15 0 0 "Subst1" 1 "3.2 mm" 1 "10 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <GND * 1 270 750 0 0 0 0>
  <Pac P2 1 270 680 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 270 330 0 0 0 0>
  <Pac P3 1 270 260 18 -26 0 1 "3" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <MLIN MS11 1 400 230 -26 15 0 0 "Subst1" 1 "3.2 mm" 1 "10 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS12 1 410 650 -26 15 0 0 "Subst1" 1 "3.2 mm" 1 "10 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MSTEP MS4 1 610 230 -26 17 0 0 "Subst1" 1 "3.2 mm" 1 "1.7 mm" 1 "Hammerstad" 0 "Kirschning" 0>
  <MSTEP MS7 1 620 650 -26 17 0 0 "Subst1" 1 "3.2 mm" 1 "1.7 mm" 1 "Hammerstad" 0 "Kirschning" 0>
  <R R1 1 500 440 15 -26 0 1 "100 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
</Components>
<Wires>
  <880 490 880 520 "" 0 0 0 "">
  <880 370 880 430 "" 0 0 0 "">
  <780 650 850 650 "" 0 0 0 "">
  <880 580 880 620 "" 0 0 0 "">
  <880 260 880 310 "" 0 0 0 "">
  <780 230 850 230 "" 0 0 0 "">
  <1090 520 1090 560 "" 0 0 0 "">
  <910 460 990 460 "" 0 0 0 "">
  <1050 460 1090 460 "" 0 0 0 "">
  <270 710 270 750 "" 0 0 0 "">
  <270 290 270 330 "" 0 0 0 "">
  <270 230 370 230 "" 0 0 0 "">
  <270 650 380 650 "" 0 0 0 "">
  <640 230 720 230 "" 0 0 0 "">
  <430 230 500 230 "" 0 0 0 "">
  <650 650 720 650 "" 0 0 0 "">
  <440 650 500 650 "" 0 0 0 "">
  <500 230 580 230 "" 0 0 0 "">
  <500 230 500 410 "" 0 0 0 "">
  <500 650 590 650 "" 0 0 0 "">
  <500 470 500 650 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
